# NanoBlog - High Frequency, Low Quality

This section is meant for byte-sized, low quality shitposting. :{{boolin}}:

- - -

#### 2023

#### Dec31

Did this year's advent of code [in lua][aoc23]. Solved nearly all the puzzles on
my own :{{based}}:. Except day 21 part 2 where I had to take some hints and day
24 part 2 which remains unsolved :{{peperope}}:.

Felt like this year's AOC wasn't as fun as last's.
Most of the puzzles didn't seem very engaging. Some of them were pretty
unsatisfying as it required you to basically observe special properties of the
input which weren't present in the example or stated in the description.

Still, I did manage to learn a couple new things.  Planning to do a write-up on
the lessons learnt. Coming soon™ hopefully.

[aoc23]: https://codeberg.org/NRK/slashtmp/src/branch/master/AoC/2023

#### Apr18

Status report: still MELTING

![melting2](pics/2023-melting2.png)

(Just noticed that all the weather stats is the exact same as yesterday's).

#### Apr17

Status report: MELTING

![melting](pics/2023-melting.png)

#### Mar28

Aright, finally got the notes section into the navbar. Removed nanoblog in the
process to make space.

#### Mar19

Been messing around with some lock-free algorithms. I kinda like it. Locks are
certainly much *easier* to get started with, but lock-less feels much more
honest and direct - it fully embraces the concurrent nature of the task.

As for C11 atomics, it's nice that there's now a standard interface for this.
However the interface is so verbose I really couldn't stand it (surprisingly the
gcc builitins are less noisy despite having double underscore prefixes).
Ended up using macros like the following:

```c
#define tomic_load(P, MO)           atomic_load_explicit((P), memory_order_##MO)
#define tomic_store(P, VAL, MO)     atomic_store_explicit((P), (VAL), memory_order_##MO)
```

Cuts down _so much_ useless noise. For example, compare the following:

```c
do { ... } while (!atomic_compare_exchange_strong_explicit(&target, &old, new, memory_order_release, memory_order_relaxed));
// vs
do { ... } while (!tomic_cas(&target, &old, new, release, relaxed));
```

YUCK. Starting to think that the C++ committee (which is where the C committee
copies everything from nowadays) are probably just a bunch of widescreen monitor
salesmen in disguise.

P.S: I've published a simple [tomic.h][] wrapper around the C11 atomics.
Feel free to use it if you want.

[tomic.h]: https://codeberg.org/NRK/slashtmp/src/branch/master/misc/tomic.h

#### Feb19

The site now has a [notes](https://nrk.neocities.org/notes.html) section where I
post various small technical stuff along with it's own RSS feed. I'm still
trying to figure out how to add "notes" to the navbar without crowding it too
much.

Also, this will be the last RSS update for nanoblog. The main rss feed will now
only contain articles, and the notes rss feed will contain notes. NanoBlog
section won't have any dedicated RSS feed (unless my shitposting ends up having
massive demand somehow).

#### Feb18

> Anyways. Please get ready for my newest proposal,
>
> printf!-ing to the SIZE\_MAX:
> %z*.s and other modifiers for precision arguments
>
> [@__phantomderp](https://twitter.com/__phantomderp/status/1620987724772810753)

Wait until he figures out `printf` returns an `int` :{{pepekek}}:

#### Jan27

Recently on an IRC channel I saw a conversation that went something like this:

person0: \*shares an article\* <br>
person1: that information there is not correct! <br>
person1: oh wait, that article is from years ago (back when it used to be true). <br>

This made me realize the value of having a publish date easily associated with
an article. It can be the difference between someone thinking your post was shit
vs sending a correction/update to an outdated information.

So I've went ahead and added a creation time to all my articles now :{{based}}:.

#### Jan09

Good thing about winter: high quality sleeps and naps.
Bad thing: your food gets cold half-way through eating.

Overall I do like winter though, it's probably my favorite season if I had to
pick.

#### Jan05

Wew lads, looks like we got ourselves a completely new and clean nanoblog page!

~~TODO: link to the previous year or make some sort of index list.~~


<p>
<br>
<span style="float:left;">
<a style="padding-left:12px"   href="nanoblog_2022.html">&lt;= 2022</a>
</span>
<span style="float:right;">
<a style="padding-right:12px"  href="nanoblog_2024.html">2024 =&gt;</a>
</span>
</p>
