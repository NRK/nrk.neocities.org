# NanoBlog - High Frequency, Low Quality

This section is meant for byte-sized, low quality shitposting. :{{boolin}}:

- - -

#### 2024

#### Oct04

___Corsair Sabre RGB Pro Review___

- ✅ Ok shape for relaxed palm grip (my hand size is around 19.5cm).
- ⛔ Too big to comfortably claw or fingertip grip.
- ⛔ The surface isn't very grippy. If you have sweaty palms then it'll start sliding.
- ⛔ The left side has decent curve (+ the side buttons) to hold onto but the right side is too flat. Grip slides off easily, especially when palms are sweating.
- ✅ Wire is fine and flexible. (The connector isn't angled up though, "ziptie mod" helps).
- ℹ️ According to [techpowerup](https://www.techpowerup.com/review/corsair-sabre-rgb-pro/5.html) it has around 6% DPI deviation. You might need to adjust your in game sens accordingly.
- ⛔ Cancer ass software that downloads gigabytes of god knows what and takes ages to "install", no portable minimalist version like Logitech's "Onboard Memory Manager". (Also no linux support, I had to use a VM + usb passthrough to change the mouse settings).
- ✅ 4khz polling and 5800 dpi works fine (I wanted to use 6400 dpi but it starts smoothing at 6000).
- ⛔ 8khz polling is gimped. You need to move the mouse insanely fast for it to actually go into 8khz. It seems to be some sort of "power saving" thing where it stays at 4khz normally and only goes to 8khz when moving fast.
- ⛔ The M1 and M2 buttons have some wobble (not a problem for me, but might annoy some).
- ✅ Side-buttons and scroll wheel are decent.
- ℹ️ Some people claimed issue with slam clicking, but I haven't had any issue with my unit.
- ℹ️ There's also non-RGB version with 5 grams less weight. Should be preferable if available (wasn't available here, so I got the RGB version).

---

Overall, it's usable but I wouldn't really recommend it mainly due to the huge ass size.
For relaxed palm grip EC2 shape is far superior and for claw/fingertip you'd want something smaller sized.

#### Aug02

Aright, an [index page for all the tags](https://nrk.neocities.org/tags) is done
now.

#### Aug01

Woah lads, another year, another new nanoblog page!

I guess this is a good time to admit that the "high frequency" part of the title
hasn't quite panned out that way.

But in other news: I've added a tagging system to my articles now 🎉.
Some of the repetitive parts of making a new post has also been semi-automated.
I'm missing a tag page which lists all the available tags though.
I'll get to it some other day...

<!-- -------------- -->

<p>
<br>
<span style="float:left;">
<a style="padding-left:12px"  href="nanoblog_2023.html">&lt;= 2023</a>
</span>
</p>
