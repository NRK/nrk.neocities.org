# NanoBlog - High Frequency, Low Quality

This section is meant for byte-sized, low quality shitposting. :{{boolin}}:

- - -

#### 2022

#### Dec25

[Since learning Tcl](#oct14) I haven't gotten too many opportunities to use it
because most of my scripting needs are already satisfied.
So I decided to do [Advent of Code][aoc] this year in Tcl to hone my skills
and managed to get all but one star [doing it entirely in Tcl][aoc22_sol].

So far, I'm _very much_ pleasantly surprised at just how capable the language is
despite being so simple.
It does have it's issues, but which language doesn't?
So the real question is, "How easy is it to workaround/overcome the limitations
and/or annoyances of the language?"
And for Tcl it's rather easy due to just how extensible the language is.

Overall I'm very glad that I finally took a look into Tcl and am very happy to
have it on my toolkit.

[aoc]: https://adventofcode.com/
[aoc22_sol]: https://codeberg.org/NRK/slashtmp/src/branch/master/AoC/2022

#### Nov15

Yet again my workflow was broken by a software update.
This time one of the mpv plugins that I use - mainly for the progress-bar and
the subtitle/audio track menu - stopped responding to mouse clicks.

Okay, well... my plugin version was quite old.
So maybe I should upgrade the plugin as well - or so I thought until I visit
their repo to see that they've basically changed everything and added a shit
load of bloat that does nothing but clutters the UI.

After failing to fix the older version, and realizing that the newer version is
going in a direction which doesn't align with my ideals - I figured it's a good
idea to just write my own.
This also gave me a nice excuse to learn lua, which turned out to be quite
simple (though I have a bone to pick with it's 1 based indexing).

The biggest pain point was trying to figure out how to draw a rectangle.
Turned out that mpv doesn't provide any drawing primitives and you need to
basically hack it together via the [ASS subtitle
format](https://aegi.vmoe.info/docs/3.1/ASS_Tags/).

And after some hours of work, most of which was spent trying to figure out the
ASS format, and mind you that I had *absolutely* no experience with lua or the
mpv api either; I've finally gotten something that I'm very happy with:
[mfpbar](https://codeberg.org/NRK/mpv-toolbox/src/branch/master/mfpbar).

<div align="center">
<a href="https://webm.red/view/WZhH.webm"><img width="60%" style="max-width:420px;" src="https://images2.imgbox.com/cb/e5/HcbVn5lT_o.png"></a>
</div>

The name is a homage to the [motherfucking website](https://motherfuckingwebsite.com/),
which seemed quite appropriate since my goal was to just get a minimal
clutter-free progress-bar.

#### Oct14

Been learning [Tcl][] recently. I like it :{{based}}:.
It's like shell scripts but simpler, less chaotic and *much* more powerful.

I'll be using it for scripts which [don't warrant being written in C][10K] but
at the same time is not trivial and/or possible to do in POSIX shell.

[Tcl]: https://www.tcl-lang.org/about/language.html
[10K]: http://www.catb.org/~esr/writings/unix-koans/ten-thousand.html

#### Jul05

After the overhaul about a week ago, I'm currently quite happy with the site's
current state. A couple other things on my TODO list:

- [x] Restructuring the homepage. It's currently bit of a mess. :{{peperope}}:
- [ ] Automatic RSS feed generation. Never bothered with it since I make very
      little posts.
- [ ] Article tagging. Not worth it as of now, but might be useful once there's
      many articles. *Has* to be static though, no jabaskript.
- [ ] Source syntax highlighting. Might look into [GNU Source-highlight][src-hl]
      and do it statically.

[src-hl]: https://www.gnu.org/software/src-highlite/source-highlight.html

#### Jun25

[Switched over from smu to md4c][switch] for generating the website. [md4c][]
has many more features, such as reference style links, tables, fenced code
blocks etc compared to [smu][]. But despite that, md4c is actually faster than
smu. Which is quite amazing because smu was already really fast.

It still has some features missing, such as automatic header id, footnotes etc;
compared to [lowdown][]. But I can write my own filters for those features. And
md4c (or smu) + my custom filters are still *much* faster lowdown.

[lowdown]: https://kristaps.bsd.lv/lowdown
[switch]: https://codeberg.org/NRK/nrk.neocities.org/commit/14ce3414292dc54b08ab84c6847caa2d1238f26b
[md4c]: https://github.com/mity/md4c

#### Jun23

Decided to spend the day writing a tool for taking reference style markdown
links and converting them to inline links instead. No doubt it's some of the
most dirtiest code I've written. Wondering if I should even push it to the
public branch or keep it a secret. :{{peperope}}:

Oh and I've been writing some other [markdown filters][] recently as well to get
around some of the shortcomings of [smu][]. Wondering if I should just write my
own markdown compiler or not. Seems like it'd be a fun thing to do.

[markdown filters]: https://codeberg.org/NRK/nrk.neocities.org/src/branch/master/filters
[smu]: https://github.com/Gottox/smu

#### May15

Spent today doing some literal *micro-optimizations*. Which means, shaving off
micro-seconds from some functions. Was worth it at the end though, since those
functions were being called in a loop.

Cpu usage spikes went down from `19.8%` -> `9.8%`. I'm thinking it's possible
to get it down even lower. But that might require some non-portable hacks.

#### Apr22

Made the zero slashed instead of dotted on [my font][NRK-Mono] about 2 weeks
ago. Can't say I like it, but can't say I hate it either. Think I'll just keep
it this way.

**TODO:** update the preview image on the repo.

#### Mar28

Wasted entire day patching fonts. Was worth it at the end though. :{{boolin}}:

Here's the final result: [NRK Mono][NRK-Mono].
\[Previews\]: [Condensed][], [Regular][].

[NRK-Mono]: https://github.com/N-R-K/NRK-Mono
[Condensed]: https://raw.githubusercontent.com/N-R-K/NRK-Mono/344eed69cd1d1604c42d52e19689fa16e0261563/NRK-Condensed.png
[Regular]: https://raw.githubusercontent.com/N-R-K/NRK-Mono/344eed69cd1d1604c42d52e19689fa16e0261563/NRK-Mono.png

#### Mar25

> I get why people want to censor sites trafficking mainly in misinformation,
> disinformation, absurd theories about how things work, blatantly
> erroneous claims, and trolling by bad actors. But it's a free
> country, and people should be able to read Hacker News if they want
> to.
>
> \- [Ceasy Muratori](https://twitter.com/cmuratori/status/1507133035132514320)

I agree. I'm not kidding or exaggerating when say that learning about
HackerNews has *completely* changed my worldview; before it, I used to think
_Reddit_ was the biggest gathering of circlejerking npc cattle.

#### Mar24

gtk is a disaster. If you're trying to search something in the file-picker,
don't press space; that'll end up selecting the file.
[5 years btw.](https://gitlab.gnome.org/GNOME/gtk/-/issues/616) :{{honkler}}:

Oh, and yes, I added emojies to the site now. :{{boolin}}:

#### Mar20

I haven't posted a whole lot because I wanted to keep my articles high quality.
This obviously means spending good amount of time researching and generally
being well knowledgeable on the subject. Which means that there's going to be
very low amount of articles posted by me.

So I decided to open up this nanoblog section, which is just going to be used
for smaller, low quality posts.

Wondering if it makes sense to have a separate rss feed for this... I think I'll
leave things be until I find someone that has a problem with it.

<p>
<br>
<span style="float:right;">
<a style="padding-right:12px" href="nanoblog_2023.html">2023 =&gt;</a>
</span>
</p>
