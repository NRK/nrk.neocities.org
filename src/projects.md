# Projects

These are some programs/scripts/libraries written by me:

* [sxcs](https://codeberg.org/NRK/sxcs) <br>
  Minimal X11 Color Picker and Magnifier.
* [sxot](https://codeberg.org/NRK/sxot) <br>
  Minimal X11 screenshot tool.
* [selx](https://codeberg.org/NRK/selx) <br>
  Minimal selection tool for X11 (can be easily used with `sxot`).
* [sxbm](https://codeberg.org/NRK/sxbm) <br>
  Simple command line bookmark manager written in POSIX compliant shell script.
* [shellmarks](https://codeberg.org/NRK/shellmarks) <br>
  Simple directory bookmark management via symlinks.
* [mpv-toolbox](https://codeberg.org/NRK/mpv-toolbox) <br>
  Scripts for the mpv video player.
  Notably includes a [progress-bar][mfpbar] and a
  [dmenu based selection menu][mdmenu].
* [nix-compress](https://codeberg.org/NRK/nix-compress) <br>
  Modern implementation of the ancient unix compress(1) tool.
* [libz1](https://codeberg.org/NRK/libz1) <br>
  A set of single header utility library for C.

[mfpbar]: https://codeberg.org/NRK/mpv-toolbox/src/branch/master/mfpbar
[mdmenu]: https://codeberg.org/NRK/mpv-toolbox/src/branch/master/mdmenu

These are a couple projects where I have notable contribution.
Sorted (mainly) by contribution volume:

* [nsxiv][] - Community fork of [sxiv][], a simple image viewer for X11. <br>
  => Currently one of the active maintainers of the project.
* [nnn][] - Efficient but powerful terminal file manager. <br>
  => Contributed various features, performance improvements and many other things.
* [dmenu][] - Graphical menu for X11. <br>
  => Mainly contributed various performance improvements.
* [imlib2][] - Feature-packed image library in C. <br>
  => Contributed various bug-fixes.
* [scrot][] - screenshot tool using imlib2. <br>
  => Helping out with the development since the project needed (and still needs)
  contributors.
* [mpv][] - scriptable and powerful media player. <br>
  => Bug fixes and some small features.
* [guru][] - Gentoo User Repository. <br>
  => If a nice software I use isn't packaged, I package it and make it available
  to other gentoo users via publishing it on `GURU`.

[nsxiv]:  https://codeberg.org/nsxiv/nsxiv
[sxiv]:   https://github.com/muennich/sxiv
[nnn]:    https://github.com/jarun/nnn
[dmenu]:  https://git.suckless.org/dmenu
[imlib2]: https://git.enlightenment.org/old/legacy-imlib2
[guru]:   https://github.com/gentoo/guru
[scrot]:  https://github.com/resurrecting-open-source-projects/scrot
[mpv]:    https://github.com/mpv-player/mpv

There are plenty of other projects where I've contributed small
bug/warning/compatibility fixes. Too many to list and I haven't kept track of
them either given they're pretty trivial things.
