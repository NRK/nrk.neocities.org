# Making my site open source

<!-- TAGS: blog -->
<ctime>03 Dec 2021</ctime>

Since I'm a proponent of [free (as in freedom) software](http://www.gnu.org/philosophy/free-sw.html),
it's only fair that my website is also free. And so, I've made the "source"
available on my [codeberg](https://codeberg.org/NRK/nrk.neocities.org).

## History

Originally I had no plans to create a website. But after seeing someone waste
30 mins trying to find a "framework" just for a static blog site I said, "You
know you could've learnt HTML and CSS and had the website up and running by
now."

Of course, I pulled that entirely out of my ass. But I don't think I was too far
off. Since I already knew basic HTML and CSS, I decided to see if I can get a
basic static site up and running in 10 mins. If I remember correctly, it took me
around 12~13, so not too far off indeed.

For a good amount of time, the site simply sat on my disk. It wasn't available
for public consumption. But after a couple months I thought, "Ehh, why not turn
it into a blog?"

## Switching over to markdown

Shortly after I made the site public, I tried to write my first article,
[Vim: view git commit log without any plugins](vim-gitlog.html), and realized
that if you intend to post code-snippets, writing a website in pure HTML and
CSS is **NOT** a good idea.

Simply put, you cannot just copy paste a code snippet into HTML. Lots of
chars which are commonly used in code, will need to be escaped and it was
overall pain.

This lead to search for some better formatting language. Initially I played
around a bit with `groff-www` but soon found out that doing code-blocks in
groff is also a pain.

Finally decided that markdown would be the way to go. However the popular
md2html converter `lowdown` was quite bloated for my needs. That's when I
realized, "Hey, the suckless guys are using markdown for their website as well.
I wonder what they use." Turns out they're using
[smu](https://github.com/Gottox/smu), which is much smaller, simpler and more
suited for what I'm doing.

However, I'm do have some small patches that I apply to `smu`. They're available
on the [site repo](https://codeberg.org/NRK/nrk.neocities.org) as well.

## Compiling/Building

To manage compilation (markdown -> html) and uploading the html to the server, I
use a simple `Makefile`. Simply running `make` will automatically update/build
any outdated html and then running `make update` will push to changes to the
remote server.

The Makefile is not entirely POSIX though, but since this is just for my
personal usage, don't think it needs to be POSIX. Just as long as it "werks on
my machine".

## Website hosting

Since this is not anything too serious, I wanted to go with a free webhosting
service. The choice of neocities over something like github pages was pretty
obvious. Neocities is open-source while github is proprietary. The websites on
neocities are also more creative and has a 2000s vibe to them.

For now, I have no plans of getting my own server. But even if I do, I think
I'll probably keep using neocities simply because I like their values and goals.

## Closing words

I'm not entirely sure on how I want to move forward with the website. So far,
I've only written technology related articles/blogposts, however I don't plan on
*strictly* keeping it that way. If I feel like it, I may write posts on other
topic as well.

As for when or how frequently I will write posts, there's no fixed schedule and
I intent on keeping that that way. While I'm not sure what the point of this
website is or will be, I **am** sure that it's definitely not about turning this
into a soulless chore where I'm writing things just for the sake of it.
