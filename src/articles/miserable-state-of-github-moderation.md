# The miserable state of Github's moderation

<!-- TAGS: blog, rant -->
<ctime>07 Jun 2022</ctime>

It all started roughly two days ago when I was greeted with the following after
trying to login [to my account](https://archive.is/7H4kf):

![](pics/ghmod00.png)

So what ToS did I violate? I have no clue. But regardless, this article isn't
about my mysterious ban, it's about the complete disastrous way the entire
situation was carried out by GitHub instead.

## Users are kept in the dark

Prior to this I did not receive any warnings or any explanation of *what
specifically* I have violated in the ToS. And mind you, GitHub's ToS isn't some
tiny 10 liner (I have skimmed through it, along with their privacy policy, way
before this incident).

Telling the user that they've been banned for "violating ToS" is the same as
arresting someone and citing "breaking the law" as a reason. It's functionally
the same as not giving any reason at all.

And this situation is especially frustrating, if the ban happened to be some
false-positive ban, as you'd be left scratching your head (like me) with no clue
*what* ToS exactly you've violated.

## No one's safe

Even if you're a [blissful soul](https://en.wikipedia.org/wiki/First_they_came_...#Text)
who thinks github doesn't have any false positive bans, and everyone getting
banned doesn't deserve to know why, and you're a perfect being who would never
face such a situation... You're still not safe.

When I said "suspended" I wasn't just talking about *my* account being affected.
No, ***every single Issues/Comments/Pull-requests opened by my account all got
silently WIPED without any notice.*** So even if *you* don't do anything wrong,
you may wake up one day and find that:

* As a developer, you are robbed from any open pull request or code review from
  the banned user, as they've gotten silently wiped.
* Not just *open* pull requests, you can no longer reference any valuable
  technical discussion that might've happened on a closed pull request from the
  banned user. Yes, ***since the entire thread is wiped***, anything that *YOU*
  or someone else might've posted on that thread is now also wiped.
* As a user of a software, you cannot view any helpful
  workaround/patches/scripts posted on an issue by the banned user.
* As a packager, your packages might silently start failing because all of that
  user's repos got wiped without any notice.

The list goes on, you get the idea. Even if we assume that the user *did* do
something wrong and deserved to banned, what's the justification for the havoc
this is going to cause to the other developers, who now have to suffer the
consequence?

The way GitHub is banning it's user isn't just a silent kidnap and execute. No,
it's a reckless brushfire with absolutely no regards to any of the bystanders
who are also getting shot.

## Can't escape the bugs

Unrelated, but I found it funny how even at the face of getting your existence
wiped, you still can't escape the buggy, unreliable, shitty mess that is modern
software.

This used to have around 15 open issues before the wipe, now there's only 5 (I'm
sure that the other developers of this project deserved to get their issue
tracker, with valuable technical discussion I might add, wiped silently!).

However notice at the top how the pinned issue escaped the eraser :)

![](pics/ghmod01.png)

*(The link does 404 upon clicking though.)*

As I was writing this article, I came across another bug. My code review on
this PR got wiped. But notice how the "conversation" tab still says "14
comments" :)

![](pics/ghmod02.png)

## Cost of broken trust

Public trust in Github has been rather low and under decline given the
following events:

* Microsoft's controversial acquisition of GitHub.
* [Take down of youtube-dl](https://itsfoss.com/youtube-dl-github-takedown/).
* [Wiping of Russian accounts](https://www.jessesquires.com/blog/2022/04/19/github-suspending-russian-accounts/)\[0].

\[0]: *I should probably clarify that I'm not Russian, have never been to
Russia, and have no affiliation with any Russian orgs*. So the reason of my ban
is probably not that.

\[0]: After revisiting this article, I just noticed that there's been an update
which states:

> According to him (Martin Woodward, the Senior Director of Developer Relations
> at GitHub), the only mechanism GitHub previously had to suspend accounts was
> built to target spammers and other malicious actors — scenarios in which,
> usually, the best thing to is make the accounts and all activity entirely
> disappear.

This makes matters worse. Because this update was posted at April 21, 2022.
Which means It's been over a month, and GitHub still hasn't resolved the issue
and is perfectly fine with wiping accounts with no regards towards the various
other developers who are getting negatively affected by this reckless action.

It's been roughly 48 hours since I've opened a support ticket (ID: 1656025).
So far I still haven't gotten any replies.

But regardless of what happens to my account, and even if GitHub were to grant
me permanent immunity from bans, moving forward it's still going to be hard if
not impossible for me (and anyone else affected by this I assume) to trust
GitHub as a development service since *any of the Pull-requests, Issues,
valuable comments of YOUR project's repository can get wiped at any moment
without any notice or care regardless of if YOU've done anything wrong or not*.

Did I mention that I had previously skimmed though the [GitHub's ToS](https://docs.github.com/en/site-policy/github-terms/github-terms-of-service#3-github-may-terminate) ?

> GitHub has the right to suspend or terminate your access to all or any part of
> the Website at any time, with or without cause, with or without notice,
> effective immediately.

## Update Jun20

As of today, roughly two weeks later, I've (finally) gotten an email from GitHub
support saying that they've reinstated my account. After asking why it was
banned to begin with, I was told it was related to my email address.

About an year ago, while I was looking for an alternative to Gmail, I used a
[cock.li](https://cock.li/) email temporarily for about a week or two (this was
back when they weren't invite-only). After I found an email provider I liked
([disroot](https://disroot.org)), I moved over and forgot all about the cock.li
account.

I'm not sure why this triggered a ban on GitHub an year later. I didn't ask
weather the ban was automatic or manual (as I didn't think it mattered much)
but I presume that they've recently started auto-flagging all suspicious emails
and I happened to got caught in it.

But in any case, everything I've said in this article still remains valid.
Specifically:

0. Users should be made aware (via email) of why *exactly* their account is
   being banned.
0. Even in the case of ToS violation, recklessly wiping accounts is incredibly
   inappropriate and results in data loss for unrelated repositories. If such
   action needs to be done, it needs to be done with care to make sure it
   doesn't damage other unrelated developers.

One good thing that came out of this situation is that at least one project
([nsxiv](https://codeberg.org/nsxiv/nsxiv)) has switched over it's development
away from GitHub and to a libre platform, [CodeBerg](https://codeberg.org).

I think more libre projects should follow suit and consider hosting themselves
at libre platforms and make sure that they do not depend on proprietary
services.

Anyhow this concludes the end of this story. And speaking of libre platforms, I
might do a write-up on my experience using CodeBerg soon™️.
