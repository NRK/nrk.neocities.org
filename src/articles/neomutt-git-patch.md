# Sending git patches with neomutt

<!-- TAGS: cli -->
<ctime>29 Apr 2022</ctime>

Even today there are many projects which still use mailing list as their main
platform of development. This means that if you're trying to send a bugfix or
feature to such a project, you'll have to send the patch via email. There's no
Pull/Merge request.

`git` already has a command for this very purpose, `git send-email`. However I
already have neomutt setup and I didn't want to bother setting up `sendmail`.

As a noob trying to look up how to use neomutt to send git patches turned out
to be surprisingly difficult. There's barely any tutorial on this topic (or
maybe I was looking up the wrong search term). So I thought I'd write this post
which could potentially save some other noobs some time.

## Generating a patch

To send a patch, first we'll need to generate it. You can skip this part if you
already know how to, for those that don't; the command you're looking for is
`git format-patch`.

In order to generate a patch for the latest commit run:

	git format-patch -1

You can also give it a commit hash to generate a patch for a specific commit:

	git format-patch -1 7269c53

And if you're working on a separate branch with multiple commits, you can
specify the branch. The following will generate a patch for every commit that's
in the current branch, but not in the `master` branch:

	git format-patch master

That's the basics, you should check out the manpage for `git-format-patch` for
more detailed information.

## OK, I have have the patch, how do I send it?

Turns out, neomutt has a flag `-H` which allows you to pick a draft file. We can
use that to send our patch to `list@example.com` now.

	neomutt -H commit.patch list@example.com

This should more or less do the trick. If you use a signature, then you should
also add `-e 'unset signature'` as an argument to remove the signature.

Finally, I also have this flag on my script, `-e 'set send_charset=utf-8'` which
seems to set the encoding to UTF-8. I'm not sure why it's there, or weather it's
needed or not, but if I remember correctly it was some sort of workaround to
some neomutt bug.

And that's basically it. In case you spot any mistakes or improvement in the
article, feel free to contact me via email about it.

**BONUS**: Ludovico Gerardi has pointed out that you can use `git config
format.to` in order to make `git` automatically add a `To:` header to the patch
file.
`neomutt` is capable of reading that header and setting it as the recipient
without having to type the address yourself manually.
