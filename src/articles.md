# Articles

## Articles and Blogposts

* [Advent of Code 2024 & BQN](articles/aoc24-bqn.html)
* [ChibiHash: Small, Fast 64 bit hash function](articles/chibihash.html)
* [nix-compress: fast LZW decoding via unrolled linked list](articles/lzw-implementation-overview.html)
* [strlcpy and how CPUs can defy common sense](articles/cpu-vs-common-sense.html)
* [Decoding UTF8 with Parallel Extract](articles/utf8-pext.html)
* [Advent of Code 2023: What I've learnt](articles/aoc23.html)
* [A gentle introduction to static analyzers for C](articles/c-static-analyzers.html)
* [Hash based trees and tries](articles/hash-trees-and-tries.html)
* [Implementing and simplifying Treap in C](articles/simple-treap.html)
* [Should you set free()-ed pointers to NULL?](articles/free-null.html)
* [A lock-free single element generic queue](articles/lockfree-1element-queue.html)
* [X11: Listening for an event with a timeout the right way](articles/x11-timeout-with-xsyncalarm.html)
* [libz1: Experience in writing my first library](articles/libz1-experience-in-writing-library.html)
* [isdigit: simple function, multiple implementation](articles/isdigit-multi-implementation.html)
* [The miserable state of Github's moderation](articles/miserable-state-of-github-moderation.html)
* [I'm not a fan of strlcpy(3)](articles/not-a-fan-of-strlcpy.html)
* [C: Stop writing f()](articles/c-stop-writing-old-function-decel.html)
* [Sending git patches with neomutt](articles/neomutt-git-patch.html)
* [Making my site open source](articles/site-open-source.html)
* [Vim: view git commit log without any plugins](articles/vim-gitlog.html)
