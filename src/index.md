# Homepage

This is my personal site where I share things which I find interesting.
As of now it is mostly tech related, but that might change in the future.

A small tour of the various section of this site:

* [Articles](articles.html) Is where I write articles about various topics.
* [Notes](notes.html) Is where I write small technical notes.
* [Projects](projects.html) Highlights some of my projects and projects I
  contribute to.
* [NanoBlog](nanoblog/index.html) Is a section for shitposting.

## Recent Articles

* [Advent of Code 2024 & BQN](articles/aoc24-bqn.html)
* [ChibiHash: Small, Fast 64 bit hash function](articles/chibihash.html)
* [nix-compress: fast LZW decoding via unrolled linked list](articles/lzw-implementation-overview.html)

## About me

* [My CodeBerg][] Is what I use for hosting my projects.
* [My GitHub][] Is used mainly for collaborating and mirroring purposes.
* [My Website](retard.html) Is just this website.

[My Codeberg]: https://codeberg.org/NRK
[My GitHub]: https://github.com/N-R-K

## Contact

* Email: nrk `AT` disroot `DOT` org

<style>
ul {
    list-style-type: none;
} ul > li:before {
    content: "=> ";
    margin-left: -1em;
}
</style>
