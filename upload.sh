#!/bin/sh

[ -z "$1" ] && exit 1

files_get() {
  for i in "$@"; do
    i="${i#output/}"
    files="-F $i=@$i ${files}"
  done
}

if [ "$1" = "--update" ]; then
  [ ! -s update.list ] && echo "nothing to update" && exit
  files_get $(sort -u update.list | tr "\n" " ")
  mv update.list update.list.old
else
  files_get "$@"
fi

cd output || exit 1
[ -n "$files" ] && curl $files \
  -H "Authorization: Bearer $(gpg -q --for-your-eyes-only --no-tty -d ../api_key.gpg)" \
  https://neocities.org/api/upload
