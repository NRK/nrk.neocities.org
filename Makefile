CC   = c99
CFLAGS = -O3 -march=native

MDCC = ./md2html.sh
PUSH = ./upload.sh --update
UPDATE_LIST = update.list

SRC = $(wildcard src/*.md src/*/*.md)
OBJ = ${SRC:.md=.html}
OBJ := ${OBJ:src/%=output/%}
OBJ += output/style.css
OBJ += output/rss.xml output/notes_rss.xml
OBJ += output/nanoblog/index.html
OBJ += output/articles/index.html
OBJ += output/tags/index.html

FILTERS = \
	filters/emoji \
	filters/heading_id \
	filters/info \
	filters/append_tags \

all: $(OBJ)

$(OBJ): $(FILTERS) $(MDCC) filters/util.tcl

output/%.html: src/%.md
	$(MDCC) $<
	@echo $@ >> $(UPDATE_LIST)
output/retard.html: output/index.html
	sed "s|<title>NRK's Homepage</title>|<title>You're literally on it\!</title>|g" \
		$< > $@
	@echo $@ >> $(UPDATE_LIST)
output/style.css: src/style.css
	cp $< $@
	@echo $@ >> $(UPDATE_LIST)
output/nanoblog/index.html: src/nanoblog/index.html
	cp $< $@
	@echo $@ >> $(UPDATE_LIST)
output/articles/index.html: src/articles/index.html
	cp $< $@
	@echo $@ >> $(UPDATE_LIST)
output/rss.xml: src/rss.xml
	cp $< $@
	@echo $@ >> $(UPDATE_LIST)
output/notes_rss.xml: src/notes_rss.xml
	cp $< $@
	@echo $@ >> $(UPDATE_LIST)
src/tags/index.md: src/tags ./tools/index-tags
	./tools/index-tags > $@

clean:
	$(RM) $(OBJ)
	find output -type f -name '*.html' -exec rm {} \;

cleantest:
	$(RM) test.html output/test.html output/*/test.html

update: all
	$(PUSH)

.PHONY: cleantest update
