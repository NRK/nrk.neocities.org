# nrk.neocities.org

This is just the source to [my personal website][s].
The sites are written in markdown and then compiled to html via [md4c][].

[s]: https://nrk.neocities.org
[md4c]: https://github.com/mity/md4c

## Dependencies

* md4c
* GNU make
* POSIX shell
* Tcl
* POSIX utilities such as `sort`, `cat`, `tr`, `sed` etc.
* `curl` and `gpg` (optional, only needed for upload.sh)

## Loicense

All the source code in this repo are released under [WTFPL](LICENSE).
Note that any multi-media files (images/videos) are not covered by this
license.
