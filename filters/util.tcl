proc regsub_cb {exp data callbackName} {
	set start 0
	while {[set match [regexp -inline -indices -start $start $exp $data]] != ""} {
		lassign [lindex $match 0] m0 m1
		set cb_args [list]
		foreach i $match {
			lassign $i i0 i1
			lappend cb_args [string range $data $i0 $i1]
		}
		set rep [$callbackName $cb_args]
		set data [string replace $data $m0 $m1 $rep]
		set start [expr {$m0 + [string length $rep]}]
	}
	return $data
}
