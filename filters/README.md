# filters

These are a couple `pre` (meant to be applied to the markdown) and `post`
(meant to be applied to the generated html [^html]) filters for compiling
markdown to html with [md4c][].

[md4c]: https://github.com/mity/md4c
[^html]: They're meant to be applied on html generated *specifically* via
`md4c` and will not work on regular html and/or html generated via other
markdown compilers.

Most of these are very much hardcoded to my preference and is most likely not
going to be useful anywhere else.

## info - post

Turns `<info-{begin,end}>` into a decent looking info bar. Example usage:

```markdown
Hello <info>.

<info-begin>

Actually, I meant to say "bye".

<info-end>
```

By default "ℹ" will be used as the info icon. This can be configured via
`MDFILTER_INFO_SRC` env var.

LIMITATION: the `info-{begin,end}` must be in their own paragraph.

## heading\_id - post

Adds linkable `id`-s to headings. Additionally adds a clickable anchor if env
var `MDFILTER_ANCHOR_CONTENT` is defined.
