#!/bin/sh

navbar() {
	case "$1" in
		"index.md"|"retard.md") home="active" ;;
		"articles.md") articles="active" ;;
		"projects.md") projects="active" ;;
		"notes.md") notes="active" ;;
	esac

	cat << EOF
<body>
<div class="navbar">
	<img src="${prefix}assets/logo.png">
	<a href="${prefix}index.html" class="$home">Home</a>
	<a href="${prefix}articles.html" class="$articles">Articles</a>
	<a href="${prefix}notes.html" class="$notes">Notes</a>
	<a href="${prefix}projects.html" class="$projects">Projects</a>
</div><br>
EOF
	unset home articles projects notes
}

head() {
	cat << EOF
<html>
<head>
	<title>$title</title>
	<link href="${prefix}style.css" rel="stylesheet" type="text/css" media="all">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="UTF-8">
	<link rel="icon" href="${prefix}assets/favicon.ico">
</head>
EOF
}

foot() {
	cat << EOF

<br><hr>
<div class="rss" style="text-align:center">
	<img src="${prefix}assets/sneed's-feed-and-seed.png">
	<a href="${prefix}rss.xml">RSS Feed</a>
	<img src="${prefix}assets/sneed's-feed-and-seed.png">
</div><hr>

</body>
</html>
EOF
}

prefilter() {
	filters/emoji |
		filters/append_tags
}

postfilter() {
	filters/heading_id |
		filters/info
}

build() {
	export MDFILTER_ANCHOR_CONTENT='<img src="https://nrk.neocities.org/assets/chain.svg"/>'
	export MDFILTER_INFO_SRC="https://nrk.neocities.org/assets/info.svg"
	prefilter < "$1" |
		md2html --fstrikethrough --ftasklists --ftables |
		postfilter
}

main() {
	[ -n "$1" ] || exit 1

	if [ "$1" = "src/index.md" ]; then
		title="NRK's Homepage"
	else
		title="$(grep -m 1 -e "^# .*$" "$1")"
		title="${title#\# }"
	fi

	output="output/${1#src/}"
	output="${output%.md}.html"
	case "${output%/*}" in
		output/*) prefix="../"
	esac
	[ -n "$test" ] && output="${output%/*}/test.html"

	head > "$output"
	navbar "${1##*/}" >> "$output"
	build "$1" >> "$output"
	foot >> "$output"
}

main "$@"
